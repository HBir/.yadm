#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Launch bar1 and bar2
MONITOR=HDMI-2 polybar bottom &
MONITOR=DP-1 polybar bottom &
MONITOR=eDP-1 polybar bottom &
MONITOR=HDMI-2 polybar top &
MONITOR=DP-1 polybar top &
MONITOR=eDP-1 polybar top &
#polybar bar2 &

echo "Bars launched..."
